//
//  ResampleFilePlayer.cpp
//  JuceBasicAudio
//
//  Created by Jack on 09/01/2018.
//
//

#include "ResampleFilePlayer.hpp"


void ResampleFilePlayer::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    resamplingAudioSource->prepareToPlay (samplesPerBlockExpected, sampleRate);
}

void ResampleFilePlayer::releaseResources()
{
    resamplingAudioSource->releaseResources();
}

void ResampleFilePlayer::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    resamplingAudioSource->getNextAudioBlock (bufferToFill);
}

void ResampleFilePlayer::setPlaybackRate (float newRate)
{
    resamplingAudioSource->setResamplingRatio(newRate);
}
