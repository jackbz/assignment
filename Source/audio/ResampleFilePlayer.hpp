//
//  ResampleFilePlayer.hpp
//  JuceBasicAudio
//
//  Created by Jack on 09/01/2018.
//
//

#ifndef ResampleFilePlayer_hpp
#define ResampleFilePlayer_hpp

#include <stdio.h>
#include "FilePlayer.h"


/**
 Simple FilePlayer class - streams audio from a file.
 */
class ResampleFilePlayer : public FilePlayer
{
public:
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void releaseResources() override;
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    
    void setPosition (float newPosition);
    
    void setPlaybackRate (float newRate);
    
private:
    AudioFormatReaderSource* currentAudioFileSource;    //reads audio from the file
    // stream, handling the
    // starting/stopping and sample-rate conversion
    
    TimeSliceThread thread;                 //thread for the transport source
    
    ResamplingAudioSource* resamplingAudioSource;
    ReverbAudioSource* reverbAudioSource;
    int fucker;
};

#endif /* ResampleFilePlayer_hpp */
