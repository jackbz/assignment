//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Tom Mitchell on 07/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_LINKEDLIST
#define H_LINKEDLIST

/**
 Template class to store and manipulate a singly-linked list of objects.
 
 This can be a linked-list of primitives or any other non-polymorhic object
 with copy constructor and assignment operators. e.g.
 
 @code
 struct MyObject
 {
 int x, y, z;
 };
 
 SinglyLinkedList<MyObject> myList;
 MyObject object0 = {0, 0, 0};
 MyObject object1 = {1, 1, 1};
 
 myList.add (object0);
 myList.add (object1);
 
 int numItems = myList.size(); // returns 2
 MyObject object2copy = myList.getLast();
 @endcode
 
 */

template <class Type>
class GizmoidList
{
public:
    /** 
     Constructor. Creates an empty list.
     */
    GizmoidList()
    {
        head = nullptr;
    }
    
    /** 
     Destructor. Clears up the contents of the list.
     */
    ~GizmoidList()
    {
        deleteAll();
    }

    
    /** 
     Returns the number of items in the list.
     @returns       the size of the list
     */
    int size() const
    {
        int total  = 0;
        
        for (Node* current = head; current != nullptr; current = current->next)
            ++total;
        
        return total;
    }
    
    /** 
     Adds an item to the end of the list.
     @param newValue value to be added to the end of the array 
     */
    void add (Type newValue)
    {
        Node* newNode = new Node;
        
        newNode->value = newValue;
        newNode->next = nullptr;
        
        if (isEmpty())
            head = newNode;
        else
            getLastNode()->next = newNode;
    }
    
    /** 
     Removes an item from the array.
     @param index the index of the element to remove
     */
    void remove (int index)
    {
        Node* current = head;// first will have the node after deletion
        
        if (current == nullptr)
            return;
        
        Node* previous = nullptr;
        while (current != nullptr)
        {
            if (index-- == 0)
            {
                if (previous == nullptr)
                    head = current->next;
                else
                    previous->next = current->next;
                
                delete current;
                break;
            }
            else
            {
                previous = current;
                current = current->next;
            }
        }
    }

    
    /** 
     Returns the item at a given index in the list, or the end if index is out of range. Don't call this if the list is not empty 
     @param index   the index of the element to be returned
     @returns       the value of the item at @index
     */
    Type get (int index) const
    {
        Node* temp = head;
        
        while (--index >= 0 && temp->next != nullptr)
            temp = temp->next;
        
        return temp->value;
    }
    
    /** Returns the item at a given index in the list. If @index is out of range it returns the last item in the list*/
    Type& operator[] (int index) const
    {
        Node* temp = head;
        
        while (--index >= 0 && temp->next != nullptr)
            temp = temp->next;
        
        return temp->value;
    }
    
    /** 
     Iterates the list, calling the delete operator on all of its elements leaving the list empty.
     */
    void deleteAll()
    {
        while (head != nullptr)
        {
            Node* temp = head;
            head = head->next;
            delete temp;
        }
    }
    
    /** 
     Returns true if the list is empty 
     @returns   true if empty
     */
    bool isEmpty() const
    {
        return head == nullptr;
    }
    
    /** 
     Reverses the list using an iterative method 
     */
    void reverse()
    {
        if (isEmpty())
            return;
        
        Node* previous = nullptr;
        Node* next;
        
        do
        {
            next = head->next;
            head->next = previous;
            previous = head;
            head = next;
        }
        while (next != nullptr);
        head = previous;
    }
    
private:
    struct Node
    {
        Type value;
        Node* next;
    };
    
    //allways check the list is not empty before calling this
    Node* getLastNode()
    {
        Node* temp = head;
        while (temp->next != nullptr)
            temp = temp->next;
        
        return temp;
    }
    
    Node* head;
};

#include <iostream>


#endif /* H_LINKEDLIST */
