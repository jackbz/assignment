//
//  Classes.cpp
//  JuceBasicWindow
//
//  Created by Jack on 29/10/2017.
//
//

#include "GizmoidDesign.hpp"
#include <math.h>




GizmoidDesign::GizmoidDesign(FilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    baseColour = Colours::hotpink;
    resizer = new ResizableBorderComponent(this, 0);
    addAndMakeVisible(resizer);
    addAndMakeVisible(playBackRateSlider);
    playBackRateSlider.setRange(0.1, 3.3);
    
}

GizmoidDesign::~GizmoidDesign()
{
    delete resizer;
}

void GizmoidDesign::paint (Graphics& g)
{
    g.setColour(baseColour);
    g.fillRect (0, 0, getWidth(), getHeight());
    g.setColour(Colours::white);
    g.fillEllipse (getWidth()/10, getHeight()/4, getWidth()/3, getHeight()/3);
    g.fillEllipse (getWidth()*0.56, getHeight()/4, getWidth()/3, getHeight()/3);
    g.setColour(Colours::black);
    g.fillEllipse (getWidth()*0.19, getHeight()*0.32, getWidth()/5, getHeight()/5);
    g.fillEllipse (getWidth()*0.6, getHeight()*0.32, getWidth()/5, getHeight()/5);
    g.fillEllipse (getWidth()*0.133, getHeight()*0.62, getWidth()*0.75, getHeight()*0.2); // mouth
    g.setColour(Colours::white);
    g.fillEllipse (getWidth()*0.19, getHeight()*0.32, getWidth()/8, getHeight()/8);
    g.fillEllipse (getWidth()*0.6, getHeight()*0.32, getWidth()/8, getHeight()/8);
    g.setColour(baseColour);
    g.fillEllipse (getWidth()*0.133, getHeight()*0.58, getWidth()*0.75, getHeight()*0.2);
    
    if (filePlayer.isPlaying() == true)
    {
        g.setColour(baseColour);
        g.fillEllipse (getWidth()*0.133, getHeight()*0.62, getWidth()*0.76, getHeight()*0.21);
        g.setColour(Colours::black);
        g.fillEllipse (getWidth()*0.42, getHeight()*0.62, getWidth()*0.18, getHeight()*0.23);
    }
    
    
}

void GizmoidDesign::mouseEnter (const MouseEvent &event)
{
    baseColour = Colours::pink;
    repaint();
}

void GizmoidDesign::mouseExit (const MouseEvent &event)
{
    baseColour = Colours::hotpink;
    repaint();
}

void GizmoidDesign::mouseDown(const MouseEvent &event)
{
    dragger.startDraggingComponent (this, event);
    dragger.startDraggingComponent (resizer, event);
    baseColour = Colours::deeppink;
    repaint();
}

void GizmoidDesign::mouseUp (const MouseEvent &event)
{
        filePlayer.setPlaying(!filePlayer.isPlaying());
        baseColour = Colours::pink;
        repaint();
}

void GizmoidDesign::mouseDrag (const MouseEvent &event)
{
    dragger.dragComponent (this, event, nullptr);
    dragger.dragComponent(resizer, event, nullptr);
}

void GizmoidDesign::resized()
{
    resizer->setBounds(0,0,getWidth(),getHeight());
    
    float range = (6.3 + (0.5 - 6.3) * (getWidth()/300.f)) *0.1945525;
    if (range > 1)
    {
        range = range*range*range*range;
    }
    playBackRateSlider.setValue(range);
    DBG (playBackRateSlider.getValue());
    filePlayer.setPlaybackRate(playBackRateSlider.getValue());
    
    

}

void GizmoidDesign::removed()
{
    filePlayer.setPlaying(false);
}

