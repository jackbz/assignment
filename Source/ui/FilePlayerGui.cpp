/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)

{
    
    
    playPositionSlider.addListener(this);
    playPositionSlider.setRange(0, 1);
    addAndMakeVisible(playPositionSlider);
    
    
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose an audio file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
}

FilePlayerGui::~FilePlayerGui()
{
    
}

//Component
void FilePlayerGui::resized()
{
   
    fileChooser->setBounds (getHeight(), 0, getWidth()-getHeight(), getHeight()/2.f);
//    playPositionSlider.setBounds(getHeight(), 70, getWidth()-getHeight(), 60);
//    
    
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
        if (filePlayer.isPlaying() == true)
        {
        startTimer(250);
        }
        else
        {
            stopTimer();
        }
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

void FilePlayerGui::sliderValueChanged (Slider* slider)
{
    if (slider == &playPositionSlider)
    {
        filePlayer.setPosition(playPositionSlider.getValue());
    }
    
}

void FilePlayerGui::timerCallback()
{
    //playPositionSlider.setValue(filePlayer.getPosition());
}


