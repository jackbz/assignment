/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) :              audio (audio_),
                                                            filePlayerGui (audio_.getFilePlayer()),
                                                            gizmoid1 (audio_.getFilePlayer()),
                                                            gizmoid2 (audio_.getFilePlayer()),
                                                            gizmoid3 (audio_.getFilePlayer()),
                                                            gizmoid4 (audio_.getFilePlayer()),
                                                            gizmoid5 (audio_.getFilePlayer()),
                                                            gizmoidNumber (2)
{
    splash = new SplashScreen ("Welcome to my app!",
                               ImageFileFormat::loadFrom (File::getCurrentWorkingDirectory().getChildFile ("splashScreen.png")),
                               true);
    
    //make splash 900x400
    
    centreWithSize(splash->getWidth(), splash->getHeight());
    
    splash->deleteAfterDelay (RelativeTime::seconds (4), false);
    
    addAndMakeVisible(filePlayerGui);
    
    addAndMakeVisible(addGizmoidButton);
    addGizmoidButton.setButtonText("Add Gizmoid!");
    addGizmoidButton.addListener(this);
    
    addAndMakeVisible(removeGizmoidButton);
    removeGizmoidButton.setButtonText("Remove Gizmoid!");
    removeGizmoidButton.addListener(this);

    
    addAndMakeVisible (gizmoid1);
    gizmoid1.setBounds(0, getHeight()/2, 60, 60);
    addAndMakeVisible (gizmoid2);
    gizmoid2.setBounds(200, getHeight()/2, 60, 60);
    
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    filePlayerGui.setBounds (0, 0, getWidth(), 200);
    addGizmoidButton.setBounds(getWidth()-100, getHeight()/2, 100, 60);
    removeGizmoidButton.setBounds(getWidth()-100, getHeight()/2+70, 100, 60);
}

void MainComponent::paint (Graphics& g)
{
    g.setColour(Colours::mediumseagreen);
    g.fillEllipse(-1200, getHeight()/2, 3000, 500);
    g.setColour(Colours::yellow);
    g.fillEllipse(getWidth()*0.7, getHeight()*0.1, 100, 100);
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &addGizmoidButton)
    {
        if (gizmoidNumber != 5)
        {
            gizmoidNumber++;
        }
        if (gizmoidNumber == 1)
        {
            addAndMakeVisible (gizmoid1);
            gizmoid1.setBounds(0, getHeight()/2, 60, 60);
        }
        if (gizmoidNumber == 2)
        {
            addAndMakeVisible (gizmoid2);
            gizmoid2.setBounds(200, getHeight()/2, 60, 60);
        }

        if (gizmoidNumber == 3)
        {
            addAndMakeVisible (gizmoid3);
            gizmoid3.setBounds(300, getHeight()/2, 60, 60);
        }
        if (gizmoidNumber == 4)
        {
            addAndMakeVisible (gizmoid4);
            gizmoid4.setBounds(400, getHeight()/2, 60, 60);
        }
        if (gizmoidNumber == 5)
        {
            addAndMakeVisible (gizmoid5);
            gizmoid5.setBounds(500, getHeight()/2, 60, 60);
        }
        DBG (gizmoidNumber);
    }
    
    if (button == &removeGizmoidButton)
    {
        if (gizmoidNumber != 0)
        {
            gizmoidNumber--;
        }
        if (gizmoidNumber == 0)
        {
            gizmoid1.removed();
            removeChildComponent(&gizmoid1);
        }
        if (gizmoidNumber == 1)
        {
            gizmoid2.removed();
            removeChildComponent(&gizmoid2);
        }
        if (gizmoidNumber == 2)
        {
            gizmoid3.removed();
            removeChildComponent(&gizmoid3);
        }
        if (gizmoidNumber == 3)
        {
            gizmoid4.removed();
            removeChildComponent(&gizmoid4);
        }
        if (gizmoidNumber == 4)
        {
            gizmoid5.removed();
            removeChildComponent(&gizmoid5);
        }
        DBG (gizmoidNumber);
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

